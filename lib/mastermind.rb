class Code
  attr_reader :pegs

  PEGS = {'r'=> :Red,
'g'=>:Green,
'b'=>:Blue,
'y'=>:Yello,
'o'=>:Orange,
'p'=>:Purple}

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(guess)
    pgs = guess.chars.map {|c| PEGS[c.downcase]}
    if (pgs.length != 4) || !(pgs.all? {|c| PEGS.values.include?(c)})
      raise "Invalid entry"
    end
    Code.new(pgs)
  end

  def self.random
    pegs = PEGS.values.sample(4)
    Code.new(pegs)
  end

  def [](i)
    pegs[i]
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)

    self.pegs == other_code.pegs
  end

  def exact_matches(input)
    matches = 0
    pegs.each.with_index do |peg, i|
      matches += 1 if self[i] == input[i]
    end
    matches
  end

  def near_matches(input)
    near_matches = 0
    pegs.each.with_index do |peg, i|
      rotation = self.pegs
      rotation[1..-1].each do |peg2|
        near_matches += 1 if  peg2 == input[i]
      end
      rotation.rotate!
    end
    near_matches
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def get_guess
    Code.parse(gets.chomp)
  end

  def display_matches(matches)

  end
end
